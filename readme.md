visualization code borrowed and modified from:

http://bl.ocks.org/rkirsling/5001347


The code processing the data was written by Jonas Neichin as part of undergraduate research 
at the McDonnell Genome Institute. That script (vaccine_design.py) relies on two others
• prediction_class.py
• call_iedb.py
which were written by the griffith lab as part of the pvac-seq package.
The rest of the code from that package can be found: https://github.com/griffithlab/pVAC-Seq

python database creation, migration, upgrade and downgrade scripts were shamelessly taken from this amazing source https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

facebook login was implemented with the help of the facebook developers site on how to manually build your own login flow on the back end and various web tutorials
https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow
https://blog.miguelgrinberg.com/post/oauth-authentication-with-flask
https://github.com/litl/rauth/tree/master/examples/facebook
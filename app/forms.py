from flask_wtf import Form
from wtforms import StringField, BooleanField, SelectField, IntegerField, FileField
from wtforms.validators import DataRequired, NumberRange, Email
import re

class RegistrationForm(Form):
	email = StringField('email', validators=[Email()])
	username = StringField('username', validators=[DataRequired()])
	password = StringField('password', validators=[DataRequired()])
	confirm_password = StringField('password_confirm', validators=[DataRequired()])


class LoginForm(Form):
	username = StringField('username', validators=[DataRequired()])
	password = StringField('password', validators=[DataRequired()])
	remember_me = BooleanField('remember_me', default=False)

class ChangePassForm(Form):
	new_password = StringField('new_password', validators=[DataRequired()])
	new_password_confirm = StringField('new_password_confirm', validators=[DataRequired()])

class NewRun(Form):
	runName = StringField('runName', validators=[DataRequired()])
	#inputFile =  allow field to upload fasta here
	sequenceInfo = StringField('sequenceInfo') # this is to copy paste info
	inputfile = FileField(u'inputfile')
	method = SelectField('method', choices=[('ann', 'ann'), ('netmhcpan', 'netmhcpan'), ('smmpmbec','smmpmbec'), ('smm', 'smm'),
	('netmhccons', 'netmhccons'), ('pickpocket','pickpocket'), ('NetMHCIIpan', 'NetMHCIIpan'), ('nn_align', 'nn_align'), ('smm_align', 'smm_align')])
	# allele = SelectField() we need this to read from
	epitopeLength = SelectField('epitopeLength', choices=[('8','8'), ('9','9'),
	('10', '10'), ('11','11'), ('12','12'), ('13','13'), ('14','14'), ('15','15'), ('16','16')])
	cutoff = IntegerField('ic50cutoff', validators=[NumberRange(min=0)], default=500)
	retries = IntegerField('iedbRetries', default=5, validators=[NumberRange(min=0, max=100)])
	temperature = IntegerField('temp', default=50000.0, validators=[NumberRange(min=1)])

	def validate(self):
		if not Form.validate(self):
			return False
		else:
			return True



from flask import Flask, session, request, flash, url_for, redirect, render_template, abort, g
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug import secure_filename
from app import app, db, login_manager
from .forms import RegistrationForm, LoginForm
from .models import User
from urllib.parse import urlparse, urljoin
import hashlib, uuid
import subprocess
import os
import sys
import time
import shutil

# code taken from flask snippet http://flask.pocoo.org/snippets/62/
# to check url's
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

# Use sha512 encryption algorithm because all the other ones are breakable or
# on the verge of breakability
def hash_and_salt(password):
    salt = uuid.uuid4().hex
    return hashlib.sha512(password.encode('utf-8') + salt.encode('utf-8')).hexdigest()

@app.route('/')
@app.route('/index')
@login_required
def index():
    user = current_user
    posts = [  # fake array of posts
        {
            'author': {'nickname': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'nickname': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html',
                           title='Home',
                           user=user,
                           posts=posts)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = hash_and_salt(form.password.data)
        newUser = User(form.username.data, hashed_password, form.email.data)
        db.session.add(newUser)
        db.session.commit()
        flash('New User Successfully Registered.')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        hashed_password = hash_and_salt(form.password.data)
        remember_me = form.remember_me.data
        registered_user = User.query.filter_by(username=username, password=hashed_password).first()
        if registered_user is None:
            flash('Username or Password is invalid', 'error')
            return redirect(url_for('login'))
        login_user(registered_user, remember_me)
        flash('Login Successful.')
        return redirect(request.args.get('next') or url_for('index'))
    return render_template('login.html', title='Login', form=form, )

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

# @app.route('/authorize/<provider>')
# def oauth_authorize(provider):
#     if not current_user.is_anonymous:
#         return redirect(url_for('login'))
#     oauth = OAuthSignIn.get_provider(provider)
#     return oauth.authorize()


# @app.route('/callback/<provider>')
# def oauth_callback(provider):
#     if not current_user.is_anonymous:
#         return redirect(url_for('index'))
#     oauth = OAuthSignIn.get_provider(provider)
#     social_id, username, email = oauth.callback()
#     if social_id is None:
#         flash('Authentication failed.')
#         return redirect(url_for('index'))
#     user = User.query.filter_by(social_id=social_id).first()
#     if not user:
#         user = User(social_id=social_id, nickname=username, email=email)
#         db.session.add(user)
#         db.session.commit()
#     login_user(user, True)
#     return redirect(url_for('index'))

@login_manager.user_loader
def load_user(id):
    if id is None or id == 'None':
        id = -1
    # print 'ID: %s, leaving load_user' % (id)
    return User.query.get(int(id))
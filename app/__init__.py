import os
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from config import basedir

app = Flask(__name__)
Bootstrap(app)
app.config.from_object('config')
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
app.config['UPLOAD_FOLDER'] = 'app/uploads'
app.config['RESULTS_FOLDER'] = 'app/results'
app.config['SOCIAL_FACEBOOK'] = {
    'consumer_key': 'facebook app id',
    'consumer_secret': 'facebook app secret'
}
app.config['SOCIAL_TWITTER'] = {
    'consumer_key': 'twitter consumer key',
    'consumer_secret': 'twitter consumer secret'
}

from app import views, models
from app import vaccine_design, call_iedb, prediction_class
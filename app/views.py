from flask import Flask, session, request, flash, url_for, redirect, render_template, abort, g, Response, send_from_directory, send_file
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug import secure_filename
from app import app, db, login_manager
from .forms import RegistrationForm, LoginForm, NewRun, ChangePassForm
from oauth import facebook
from .models import User, Run
try:
  from urllib.parse import urlparse, urljoin
except ImportError:
  from urlparse import urlparse, urljoin
import hashlib, uuid
import subprocess
import os
import sys
import time
import shutil
import json
import re

@app.route("/set_value")
def set_value():
    session["key"] = "value"
    return "ok"

@app.route("/get_value")
def get_value():
    return session["key"]


# code taken from flask snippet http://flask.pocoo.org/snippets/62/
# to check url's
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

# http://jasonwyatt.co/post/40138193838/generate-hashed-passwords-and-salts-with-python
# wanted to do this right so took some code from the above source
def hash_password(password, salt=None):
    if salt is None:
        salt = uuid.uuid4().hex

    hashed_password = hashlib.sha512(password.encode('utf-8') + salt.encode('utf-8')).hexdigest()

    return (hashed_password, salt)


def verify_password(password, hashed_password, salt):
    re_hashed, salt = hash_password(password, salt)

    return re_hashed == hashed_password


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html',
                           title='Home',
                           user=current_user)


@app.route('/newrun', methods=['GET', 'POST'])
def newrun():
  #import pdb; pdb.set_trace()
  form = NewRun()
  if request.method == "POST":
    
    if form.validate():
      if 'inputfile' not in request.files:
            app.logger.error("No file part")
            flash('No file part')
            return redirect(request.url)
      if form.inputfile.data:
        file = request.files['inputfile']
        file_data = form.inputfile.data
        filename = form.runName.data
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file_data.save(os.path.join(filepath))

        # user = _get_user()
        # app.logger.error(user)

        # need to change allele thing here
        stream = subprocess.Popen(['app/vaccine_design.py', str(form.runName.data), str(filepath), str(form.method.data), 'H-2-Kb', '-o', app.config['RESULTS_FOLDER'],
          '-l', str(form.epitopeLength.data), '-k', 'True'], shell=False)
        
        while stream.poll() is None:
          render_template('processing.html', title="Processing")

        output = Run(filepath, os.path.join(app.config['RESULTS_FOLDER'], str(form.runName.data)), current_user.id)
        outputpath = os.path.join(app.config['RESULTS_FOLDER'], str(form.runName.data))
        results_filename = filename + '_results.fa'
        db.session.add(output)
        db.session.commit()
      return redirect(url_for('.results', outputpath=outputpath, results_filename=results_filename))
    else:
      return render_template('input_data.html',
                  title='New Run',
                  form=form)
  elif request.method == "GET":
    return render_template('input_data.html',
                  title='New Run',
                  form=form)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/processing', methods=['GET', 'POST'])
def processing():
  time.sleep(0.5)
  render_template('processing.html', title="Processing")
  # here we can put a progress bar and flash stderr
  return redirect('/results')

@app.route('/results', methods=['GET', 'POST'])
def results():
  for run in Run.query.filter_by(user_id=current_user.id).order_by(Run.timestamp):
    app.logger.error(run)

  output = request.args['outputpath']
  app.logger.error(output)
  filepath = request.args['results_filename']
  f = open(os.path.join(output, filepath))
  nodes = parseNodes(os.path.join(output, filepath))
  #import pdb; pdb.set_trace()
  return render_template('vis.html', nodes=nodes, output=output, filepath=request.args['outputpath'])


def parseNodes(input):
  seqInfo = re.compile(r'[>]')
  sequences = list()
  graph = {}
  spacers = ["HH", "HHC", "HHH", "HHHD", "HHHC", "AAY", "HHHH", "HHAA", "HHL", "AAL"]
  with open(input, 'r') as file:
    for line in file:
      if seqInfo.match(line) is not None:
        linedata = line.split('|')
        seqIDs = linedata[0].split(',')
        med_score = linedata[1]
        lowest_score = linedata[2]
        all_scores = linedata[3].split(',')
      elif line.rstrip() not in spacers:
        line.rstrip()
        sequences.append(line)
        app.logger.error("line:" + str(line))
    app.logger.error(seqIDs)
    sequence = str("sequence")
    j = 0
    for i in range(0, len(seqIDs)):
      try:
        seqIDs[i].replace('>','')
        if seqIDs[i] in spacers:
          continue
        elif seqIDs[i] not in spacers and seqIDs[i+1] not in spacers:
          graph[seqIDs[i]] = {}
          graph[seqIDs[i]][sequence] = {}
          app.logger.error(str(seqIDs[i]) + "  " + str(sequences[0]))
          graph[seqIDs[i]][sequence] = sequences.pop(0).rstrip()
          graph[seqIDs[i]][seqIDs[i+1]] = {}
          graph[seqIDs[i]][seqIDs[i+1]]['spacer'] = None
          graph[seqIDs[i]][seqIDs[i+1]]['score'] = all_scores.pop(0).rstrip()
          ++j
        elif seqIDs[i] not in spacers and seqIDs[i+1] in spacers:
          graph[seqIDs[i]] = {}
          graph[seqIDs[i]][sequence] = {}
          graph[seqIDs[i]][sequence] = sequences.pop(0).rstrip()
          graph[seqIDs[i]][seqIDs[i+2]] = {}
          graph[seqIDs[i]][seqIDs[i+2]]['spacer'] = seqIDs[i+1]
          graph[seqIDs[i]][seqIDs[i+2]]['score'] = all_scores.pop(0).rstrip()
          app.logger.error(str(seqIDs[j]) + "  " + str(sequences[0]))
          ++j

      except IndexError as e:
        app.logger.error(e)
        continue
  return graph

@app.route('/download', methods=['GET', 'POST'])
def download():
  app.logger.error(request.args.get('path'))
  file = str(os.path.basename(request.args.get('path'))) + '_results.fa'
  app.logger.error(file)
  return send_file(os.path.join('results', os.path.basename(request.args.get('path')), file), as_attachment=True)


@app.route('/vis', methods=['GET', 'POST'])
def vis():
  return render_template('vis.html', title="Vis")

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        username = form.username.data
        user = User.query.filter_by(username=username).first()
        if user is not None:
            flash('Username is taken, please enter another one')
            return render_template('register.html', title='Register', form=form)
        if form.password.data == form.confirm_password.data:
            hashed_password, salt = hash_password(form.password.data)
            newUser = User(form.username.data, None, hashed_password, salt, form.email.data)
            db.session.add(newUser)
            db.session.commit()
            flash('New User Successfully Registered.')
            return redirect(url_for('login'))
        flash('Passwords did not match, please try again')
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        unhashed_password = form.password.data
        remember_me = form.remember_me.data
        registered_user = User.query.filter_by(username=username).first()
        #import pdb; pdb.set_trace()
        if not registered_user is None:
            if not verify_password(unhashed_password, registered_user.password, registered_user.salt):
                flash('Username or Password is invalid', 'error')
                return redirect(url_for('login'))
            login_user(registered_user, remember_me)
            flash('Login Successful.')
            return redirect(request.args.get('next') or url_for('index'))
        flash('Username or Password is invalid', 'error')
    return render_template('login.html', title='Login', form=form)

@app.route('/facebook/login')
def fb_login():
    redirect_uri = url_for('authorized', _external=True)
    params = {'redirect_uri': redirect_uri}
    return redirect(facebook.get_authorize_url(**params))


@app.route('/facebook/authorized')
def authorized():
    # check to make sure the user authorized the request
    if not 'code' in request.args:
        flash('You did not authorize the request')
        return redirect(url_for('login'))

    # make a request for the access token credentials using code
    redirect_uri = url_for('authorized', _external=True)
    data = dict(code=request.args['code'], redirect_uri=redirect_uri)

    def new_decoder(payload):
        return json.loads(payload.decode('utf-8'))

    session = facebook.get_auth_session(data=data, decoder=new_decoder)

    # the "me" response
    me = session.get('me').json()

    user = User.get_or_create(me['name'], me['id'])
    login_user(user)
    flash('Logged in as ' + me['name'])
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User %s not found.' % username)
        return redirect(url_for('index'))
    old_runs = Run.query.filter_by(user_id=user.id)
    #import pdb; pdb.set_trace()
    file_names = []
    constructed_urls = []
    for run in old_runs:
        filepath = run.resultspath #app/results/filename
        result_name = '%s%s' % (filepath.split("/")[2], '_results.fa')
        file_names.append(result_name)
        constructed_urls.append(url_for('.results', outputpath=filepath, results_filename=result_name))
    return render_template('user.html',
                           user=user,
                           file_names=file_names,
                           constructed_urls=constructed_urls)

@app.route('/user/<username>/delete_acct')
@login_required
def delete_acct(username):
    user_to_delete = User.query.filter_by(username=username).first()
    if user_to_delete is None:
        app.logger.error('User \'%s\' does not exist, deletion failed' % username)
        return redirect(url_for('user', username=current_user.username))
    runs_to_delete = Run.query.filter_by(user_id=user_to_delete.id)
    for run in runs_to_delete:
        db.session.delete(run)
    logout_user()
    db.session.delete(user_to_delete)
    db.session.commit()
    flash('Account deleted successfully')
    return redirect(url_for('index'))


@app.route('/user/<username>/change_password', methods=['GET', 'POST'])
@login_required
def change_password(username):
    form = ChangePassForm()
    if form.validate_on_submit():
        if form.new_password.data == form.new_password_confirm.data:
            user = User.query.filter_by(username=username).first()
            if not user is None:
                user.password, user.salt = hash_password(form.new_password.data)
                db.session.commit()
                flash('Password successfully updated')
                return redirect(url_for('user', username=username))
        flash('Passwords did not match, please try again')
    return render_template('change_password.html', form=form)


@login_manager.user_loader
def load_user(id):
    if id is None or id == 'None':
        id = -1
    # print 'ID: %s, leaving load_user' % (id)
    return User.query.get(int(id))
from app import db, login_manager
from flask import flash


class User(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, unique=True, autoincrement=True)
    username = db.Column('username', db.String(128), index=True, unique=True)
    password = db.Column('password', db.String(128), nullable=True)
    salt = db.Column('salt', db.String(128), nullable=True)
    email = db.Column(db.String(120), index=True, nullable=True)
    fb_id = db.Column(db.String(120), nullable=True)

    def __init__(self, username, fb_id=None, hashed_password=None, salt=None, email=None):
        self.username = username
        self.password = hashed_password
        self.salt = salt
        self.email = email
        self.fb_id = fb_id

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)

    @staticmethod
    def get_or_create(username, fb_id):
        user = User.query.filter_by(username=username).first()
        if user is None:
            user = User(username, fb_id)
            db.session.add(user)
            db.session.commit()
            flash('%s Successfully logged in!' % username)
        return user


class Run(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    inputdatapath = db.Column(db.Text())
    resultspath = db.Column(db.Text())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.DateTime)

    users = db.relationship(User)

    def __init__(self, inputdatapath, resultspath, user_id):
        self.inputdatapath = inputdatapath
        self.resultspath = resultspath
        self.user_id = user_id

    def get_resultspath(self):
        return str(self.resultspath)

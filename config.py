import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

WTF_CSRF_ENABLED = True
SECRET_KEY = 'd41d8cd98f00b204e9800998ecf8427e'

FB_CLIENT_ID = '1842775515985728'
FB_CLIENT_SECRET = 'a5ccaf4669407ec41e760e02eb2dde53'